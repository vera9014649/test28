<?php

use App\Http\Controllers\Api\BrandController;
use App\Http\Controllers\Api\CarController;
use App\Http\Controllers\Api\ModelTypeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::resource('cars', CarController::class);
Route::prefix('brands')->group(function () {
    Route::get('/', [BrandController::class, 'index']);
    Route::post('/', [BrandController::class, 'store']);
});

Route::prefix('models')->group(function () {
    Route::get('/', [ModelTypeController::class, 'index']);
    Route::post('/', [ModelTypeController::class, 'store']);
});

Route::any('{any}', function () {
    return response()->json(['message' => 'Not Found'], 404);
})->where('any', '.*');
