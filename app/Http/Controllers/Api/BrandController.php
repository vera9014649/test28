<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\BrandRequest;
use App\Models\Brand;
use Illuminate\Http\Response;

class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::all();
        return response()->json(['data' => $brands], Response::HTTP_OK);
    }

    public function store(BrandRequest $request)
    {
        $brand = Brand::create($request->all());
        return response()->json(['data' => $brand], Response::HTTP_CREATED);
    }
}
