<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CarRequest;
use App\Models\Brand;
use App\Models\Car;
use App\Models\ModelType;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;

class CarController extends Controller
{

    public function index()
    {
        $cars = Car::all();

        return response()->json(['data' => $cars], Response::HTTP_OK);
    }

    public function store(CarRequest $request)
    {
        $modelType = ModelType::find($request->model_type_id);

        if ($modelType->brand_id != $request->brand_id) {

            return response()->json(['message' => 'Model type does not correspond to the brand.'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $car = Car::create($request->all());

        return response()->json(['data' => $car, 'message' => 'Car is created successfully.'], Response::HTTP_CREATED);
    }

    public function show(string $id)
    {
        try {
            $car = Car::findOrFail($id);

            return response()->json(['data' => $car], Response::HTTP_OK);
        } catch (ModelNotFoundException $e) {

            return response()->json(['error' => 'Car not found'], 404);
        }
    }

    public function update(CarRequest $request, string $id)
    {
        try {
            $modelType = ModelType::find($request->model_type_id);

            if ($modelType->brand_id != $request->brand_id) {

                return response()->json(['message' => 'Model type does not correspond to the brand.'], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $car = Car::findOrFail($id);
            $car->update($request->all());

            return response()->json(['data' => $car, 'message' => 'Car is updated successfully.'], Response::HTTP_OK);
        } catch (ModelNotFoundException $e) {

            return response()->json(['error' => 'Car not found'], 404);
        }
    }

    public function destroy(string $id)
    {
        try {
            $car = Car::findOrFail($id);
            $car->delete();

            return response()->json(['message' => 'Car deleted successfully.'], Response::HTTP_OK);
        } catch (ModelNotFoundException $e) {

            return response()->json(['error' => 'Car not found'], 404);
        }
    }
}
