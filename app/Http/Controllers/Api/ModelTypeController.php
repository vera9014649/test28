<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ModelTypeRequest;
use App\Models\ModelType;
use Illuminate\Http\Response;

class ModelTypeController extends Controller
{
    public function index()
    {
        $modelTypes = ModelType::all();
        return response()->json(['data' => $modelTypes], Response::HTTP_OK);
    }

    public function store(ModelTypeRequest $request)
    {
        $modelType = ModelType::create($request->all());
        return response()->json(['data' => $modelType], Response::HTTP_CREATED);
    }
}
